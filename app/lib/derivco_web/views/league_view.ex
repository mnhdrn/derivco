defmodule DerivcoWeb.LeagueView do
  use DerivcoWeb, :view
  alias DerivcoWeb.LeagueView

  @moduledoc false

  def render("index.json", %{leagues: leagues}) do
    %{data: render_many(leagues, LeagueView, "league.json")}
  end

  def render("league.json", %{league: league}) do
    %{
      league: league.league,
      season: league.season
    }
  end
end
