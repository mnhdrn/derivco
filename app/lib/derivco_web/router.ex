defmodule DerivcoWeb.Router do
  use DerivcoWeb, :router

  @moduledoc """
  This is module is where all the route to our application is generated
  """

  @doc """
  This is our basic scope to get some front-end interface
  """
  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  @doc """
  This is our API scope, this is the heart of this application,
  because it map our route to our controller logic to return data.
  """
  pipeline :api do
    plug :accepts, ["json", "x-protobuf"]
  end

  @doc false
  scope "/", DerivcoWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", DerivcoWeb do
    pipe_through :api

    get "/v1/matches", MatchController, :index
    get "/v1/match", MatchController, :index

    get "/v1/leagues", LeagueController, :index
    get "/v1/league", LeagueController, :index

    get "/v1/seasons", SeasonController, :index
    get "/v1/season", SeasonController, :index

    get "/v1/results", ResultController, :index
    get "/v1/result", ResultController, :index

  end
end
