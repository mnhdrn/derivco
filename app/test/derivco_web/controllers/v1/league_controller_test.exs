defmodule DerivcoWeb.LeagueControllerTest do
  use DerivcoWeb.ConnCase

  describe "index" do
    test "lists all leagues with default format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/leagues")
      resp2 = get(conn, "/api/v1/league")

      resp3 = get(conn, "/api/v1/leagues?format=testtest")
      resp4 = get(conn, "/api/v1/league?format=testtest")

      assert resp1.status == 200
      assert resp2.status == 200
      assert resp3.status == 200
      assert resp3.status == 200

      assert response_content_type(resp1, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp2, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp3, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp4, :json) == "application/json; charset=utf-8"
    end

    test "lists all leagues with json format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/leagues?format=json")
      resp2 = get(conn, "/api/v1/league?format=json")

      assert resp1.status == 200
      assert response_content_type(resp1, :json) == "application/json; charset=utf-8"

      assert resp2.status == 200
      assert response_content_type(resp2, :json) == "application/json; charset=utf-8"
    end

    test "lists all leagues with protobuf format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/leagues?format=protobuf")
      resp2 = get(conn, "/api/v1/league?format=protobuf")

      assert resp1.status == 200
      assert response_content_type(resp1, :proto) == "application/x-protobuf; charset=utf-8"

      assert resp2.status == 200
      assert response_content_type(resp2, :proto) == "application/x-protobuf; charset=utf-8"
    end
  end

  describe "format" do
    test "is format work as attended with parameters ?" do
      p1 = [
        %{league: "sp1", season: "201516"},
        %{league: "sp1", season: "201617"},
        %{league: "sp1", season: "201718"},
        %{league: "sp1", season: "201819"},
      ]
      value = [
        %{league: "SP1",
          season: [
            "201516",
            "201617",
            "201718",
            "201819"
          ]
        }
      ]
      ret = DerivcoWeb.LeagueController.format(p1)

      assert ret == value
    end

    test "is format work as attended with empty parameters ?" do
      p1 = []
      ret = DerivcoWeb.LeagueController.format(p1)

      assert ret == []
    end
  end

end
