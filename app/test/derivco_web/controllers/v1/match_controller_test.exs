defmodule DerivcoWeb.MatchControllerTest do
  use DerivcoWeb.ConnCase

  describe "index" do
    test "lists all matches with default format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/matches")
      resp2 = get(conn, "/api/v1/match")

      resp3 = get(conn, "/api/v1/matches?format=testtest")
      resp4 = get(conn, "/api/v1/match?format=testtest")

      assert resp1.status == 200
      assert resp2.status == 200
      assert resp3.status == 200
      assert resp3.status == 200
      assert response_content_type(resp1, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp2, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp3, :json) == "application/json; charset=utf-8"
      assert response_content_type(resp4, :json) == "application/json; charset=utf-8"
    end

    test "lists all matches with json format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/matches?format=json")
      resp2 = get(conn, "/api/v1/match?format=json")

      assert resp1.status == 200
      assert response_content_type(resp1, :json) == "application/json; charset=utf-8"

      assert resp2.status == 200
      assert response_content_type(resp2, :json) == "application/json; charset=utf-8"
    end

    test "lists all matches with protobuf format", %{conn: conn} do
      resp1 = get(conn, "/api/v1/matches?format=protobuf")
      resp2 = get(conn, "/api/v1/match?format=protobuf")

      assert resp1.status == 200
      assert response_content_type(resp1, :proto) == "application/x-protobuf; charset=utf-8"

      assert resp2.status == 200
      assert response_content_type(resp2, :proto) == "application/x-protobuf; charset=utf-8"
    end
  end

end
