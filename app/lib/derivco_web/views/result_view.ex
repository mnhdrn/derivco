defmodule DerivcoWeb.ResultView do
  use DerivcoWeb, :view
  alias DerivcoWeb.ResultView

  @moduledoc false

  def render("index.json", %{results: results}) do
    %{data: render_many(results, ResultView, "result.json")}
  end

  def render("result.json", %{result: result}) do
    %{id: result.id,
      league: result.league,
      season: result.season,
      date: result.date,
      games: result.games,
      winner: result.winner,
      results: result.results
    }
  end
end
