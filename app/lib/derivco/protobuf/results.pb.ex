defmodule ProtoResults do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          result: [ProtoResult.t()]
        }
  defstruct [:result]

  field :result, 1, repeated: true, type: ProtoResult
end

defmodule ProtoResult do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer,
          date: String.t(),
          league: String.t(),
          season: String.t(),
          game: String.t(),
          results: String.t(),
          winner: String.t()
        }
  defstruct [:id, :date, :league, :season, :game, :results, :winner]

  field :id, 1, type: :int64
  field :date, 2, type: :string
  field :league, 3, type: :string
  field :season, 4, type: :string
  field :game, 5, type: :string
  field :results, 6, type: :string
  field :winner, 7, type: :string
end

defmodule ProtoResultsProcess do

  @moduledoc """
  This module is complementary modules I made to separate the encoding logic
  from the controller, so all logic relative to Results protocol buffer is here.
  """

  @doc """
  encode/1 will encode a Enumerable data we retrieved for a query,
  it mean to be used with Soccer.composable_query
  """

  # Encode take a enumarable objet,
  # we throw the raw argument to build during the building of
  # our protocol buffers struct.
  # then we pipe it to encode, and base64 encode so it ready
  # to go through the API render

  def encode(results) do
    ProtoResults.new(result: build(results))
    |> ProtoResults.encode
    |> Base.encode64
  end

  @doc """
  build, is a staging function that take the raw data parameter from ProtoResultsProcess.encode
  and make it ready to be use with ProtoResults.new
  """

  # Build, will loop through our element and create a struct
  # with the ProtoResult.new function.
  # So it return an array of ProtoResult struct ready to be
  # embed in ProtoResults struct

  def build(results) do
    Enum.map(results, fn(e) ->
      ProtoResult.new(
        id: e.id,
        date: e.date,
        league: e.league,
        season: e.season,
        game: e.games,
        results: e.results,
        winner: e.winner
      )
    end)
  end

end
