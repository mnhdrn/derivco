defmodule DerivcoWeb.SeasonView do
  use DerivcoWeb, :view
  alias DerivcoWeb.SeasonView

  @moduledoc false

  def render("index.json", %{seasons: seasons}) do
    %{data: render_many(seasons, SeasonView, "season.json")}
  end

  def render("season.json", %{season: season}) do
    %{
      season: season.season,
      league: season.league
    }
  end
end
