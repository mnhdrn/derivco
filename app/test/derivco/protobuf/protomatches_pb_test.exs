defmodule ProtoMatchTest do
  use Derivco.DataCase

  alias Derivco.Soccer

  describe "ProtoMatch.new" do
    test "testing with a parameter" do
      value = %ProtoMatch{
        away: "paris",
        date: "15-03-2019",
        ftag: 0,
        fthg: 0,
        ftr: "d",
        home: "barcelona",
        htag: 0,
        hthg: 0,
        htr: "d",
        id: 5,
        league: "coding league",
        season: "201920"
      }

      ret = ProtoMatch.new(away: "paris",
        date: "15-03-2019",
        ftag: 0,
        fthg: 0,
        ftr: "d",
        home: "barcelona",
        htag: 0,
        hthg: 0,
        htr: "d",
        id: 5,
        league: "coding league",
        season: "201920"
      )

      assert ret == value
    end

    test "testing without a parameter" do
      value = %ProtoMatch{
        away: "",
        date: "",
        ftag: 0,
        fthg: 0,
        ftr: "",
        home: "",
        htag: 0,
        hthg: 0,
        htr: "",
        id: 0,
        league: "",
        season: ""
      }

      assert ProtoMatch.new() == value
    end
  end

end

defmodule ProtoMatchesProcessTest do
  use Derivco.DataCase

  alias Derivco.Soccer

  describe "ProtoMatchesProcess.encode test" do
    test "first 50 character from list_matches" do
      value = "CjcIARIKMTktMDgtMjAxNhoDc3AxIgYyMDE2MTcqCWxhIGNvcnV"
      ret = ProtoMatchesProcess.encode(Soccer.list_matches) |> String.slice(0..50)

      assert ret == value
    end

    test "with none" do
      ret = ProtoMatchesProcess.encode(%{})

      assert ret == ""
    end
  end

end
