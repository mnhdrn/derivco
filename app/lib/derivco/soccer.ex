defmodule Derivco.Soccer do
  import Ecto.Query, warn: false
  alias Derivco.Repo

  alias Derivco.Soccer.Match

  @moduledoc """
  The Soccer ecto context module
  """

  # This module, is the ecto context file,
  # Here we declare all our function that will query the tables Marches from our database
  # For this API we only got one table, so all queries logics will be present
  # in this file for our controller

  @doc """
  Returns the list of matches.

  ## Examples

  iex> list_matches()
  [%Match{}, ...]

  """

  # list_matches use Cachex function to cache the query
  # otherwise it just use the builtin Ecto Repo.all(Match)
  # to get all matches data

  def list_matches do
    Cachex.fetch!(:api_cache, "matches", fn ->
      Repo.all(Match)
    end)
  end

  @doc """
  Return a list of Seasons with an array of associated league

  ## Examples

  iex> list_seasons()
  [%{seasons: "001122", league: "liga"}, ...]

  """

  # list_seasons use Cachex function to cache the query
  # The query is built with Ecto.query method

  def list_seasons do
    Cachex.fetch!(:api_cache, "seasons", fn ->
      from(m in Match,
        select: %{league: m.league, season: m.season},
        group_by: [m.season, m.league],
        order_by: [m.season]
      )
      |> Repo.all
    end)
  end

  @doc """
  Return a list of League with an array of associated season

  ## Examples

  iex> list_leagues()
  [ %{league: "liga", season: "001122"}, ...]

  """

  # list_seasons use Cachex function to cache the query
  # The query is built with Ecto.query method

  def list_leagues do
    Cachex.fetch!(:api_cache, "leagues", fn ->
      from(m in Match,
        select: %{league: m.league, season: m.season},
        group_by: [m.league, m.season],
        order_by: [m.league]
      )
      |> Repo.all
    end)
  end

  @doc """
  Return a list of matching results.
  list_results takes two argument to create a query with.
  the function composable_query.

  So it results varies from it's given parameters

  iex> list_results(Match, [%{"team", "barcelona"}])
  [%Match{}, ...]

  """

  # this function is call for Results API endpoint
  # It use cachex like the other Soccer function
  # we loop through params to build our query
  # and pipe it in Repo.all

  def list_results(query, params) do
    verify_cache(params)
    Cachex.fetch!(:api_cache, "results", fn ->
      Enum.reduce(params, query, fn({key, value}, query) ->
        composable_query(query, key, String.downcase(value))
      end) |> Repo.all
    end)
  end

  # this private function will store the parameters if the cache is not
  # initialised. After it will compare the current parameters with the cached one.
  # If they differs it update the params cache and delete the results cache.
  # So it force list_results to re-do the query

  defp verify_cache(params) do
    ret = Cachex.fetch!(:api_cache, "results_params", fn ->
      params
    end)
    if params != ret do
      Cachex.put(:api_cache, "results_params", params)
      Cachex.del(:api_cache, "results")
    end
  end

  # this private function is a just here to separate logic
  # and avoid to much depth in list_results
  # We change the query for some parameters in order to have
  # a more acurate return

  defp composable_query(query, key, value) do
    case String.downcase(key) do
      "team" ->
        from m in query,
        where: like(field(m, :home), ^"%#{value}") or like(field(m, :away), ^"%#{value}")
      "date" ->
        from m in query,
        where: like(m.date, ^"%#{value}%")
      n when n in  ["seasons", "season"] ->
        from m in query,
        where: like(field(m, :season), ^"%#{value}%")
      _ ->
        from m in query,
        where: field(m, ^String.to_atom(key)) == ^value and not is_nil(m.ftr) and not is_nil(m.htr)
    end
  end

end
