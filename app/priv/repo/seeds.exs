# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Derivco.Repo.insert!(%Derivco.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Derivco.Repo
alias Derivco.Soccer.Match

defmodule Derivco.Seeds do

  def store_it(row) do
    changeset = Match.changeset(%Match{}, row)
    Repo.insert!(changeset)
  end

end

File.stream!("./priv/repo/Data.csv")
  |> Stream.drop(1)
  |> CSV.decode(headers: [:id, :league, :season, :date, :home, :away, :fthg, :ftag, :ftr, :hthg, :htag, :htr])
  |> Enum.map(fn({_val, %{id: _id, league: league, season: season, date: date, home: home, away: away, fthg: fthg, ftag: ftag, ftr: ftr, hthg: hthg, htag: htag, htr: htr}}) ->
    %{league: String.downcase(league),
      season: season,
      home: String.downcase(home),
      date: String.replace(date, "/", "-"),
      away: String.downcase(away),
      fthg: fthg,
      ftag: ftag,
      ftr: String.downcase(ftr),
      hthg: hthg,
      htag: htag,
      htr: String.downcase(htr)
    }
  end)
  |> Enum.each(&Derivco.Seeds.store_it/1)
