defmodule ProtoResultTest do
  use Derivco.DataCase

  alias Derivco.Soccer
  alias Derivco.Soccer.Match

  describe "ProtoResult.new" do
    test "testing with a parameter" do

      value = %ProtoResult{
        id: 4,
        date: "20-08-2016",
        game: "Granada - Villarreal",
        league: "SP1",
        results: "1 - 1",
        season: "201617",
        winner: "Drawn"
      }

      ret = ProtoResult.new(id: 4, date: "20-08-2016", game: "Granada - Villarreal",
        league: "SP1", results: "1 - 1", season: "201617", winner: "Drawn"
      )

      assert ret == value
    end

    test "testing without a parameter" do
      value = %ProtoResult{
        id: 0,
        date: "",
        game: "",
        league: "",
        results: "",
        season: "",
        winner: ""
      }

      assert ProtoResult.new() == value
    end
  end

end

defmodule ProtoResultsProcessTest do
  use Derivco.DataCase

  alias Derivco.Soccer
  alias Derivco.Soccer.Match

  describe "ProtoResultsProcess.encode test" do

    test "first 50 character from list_results" do
      value = "CkAIARIKMTktMDgtMjAxNhoDU1AxIgYyMDE2MTcqEUxhIGNvcnV"
      data = Match
             |> Soccer.list_results([])
             |> DerivcoWeb.ResultController.format
      ret = ProtoResultsProcess.encode(data) |> String.slice(0..50)

      assert ret == value
    end

    test "with none" do
      ret = ProtoResultsProcess.encode(%{})

      assert ret == ""
    end

  end

end
