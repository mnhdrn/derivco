defmodule Derivco.Repo do
  use Ecto.Repo,
    otp_app: :derivco,
    adapter: Ecto.Adapters.Postgres

  @moduledoc """
  This module manage the connection to the database
  """
end
