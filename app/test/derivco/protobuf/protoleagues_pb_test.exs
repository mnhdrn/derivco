defmodule ProtoLeagueTest do
  use Derivco.DataCase

  alias Derivco.Soccer

  describe "ProtoLeague.new" do
    test "testing with a parameter" do
      value = %ProtoLeague{
        league: "SP1",
        season: ["201516", "201617"]
      }

      ret = ProtoLeague.new(league: "SP1", season: ["201516", "201617"])

      assert ret == value
    end

    test "testing without a parameter" do
      value = %ProtoLeague{
        league: "",
        season: []
      }

      assert ProtoLeague.new() == value
    end
  end

end

defmodule ProtoLeaguesProcessTest do
  use Derivco.DataCase

  alias Derivco.Soccer

  describe "ProtoLeaguesProcess.encode test" do

    test "first 50 character from list_matches" do
      value = "Cg4KAkQxEggKBjIwMTYxNwoOCgJFMBIICgYyMDE2MTcKGQoDU1A"
      ret = ProtoLeaguesProcess.encode(Soccer.list_leagues
            |> DerivcoWeb.LeagueController.format)
            |> String.slice(0..50)

      assert ret == value
    end

    test "with none" do
      ret = ProtoLeaguesProcess.encode(%{})

      assert ret == ""
    end
  end

end
