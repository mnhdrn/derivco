defmodule ProtoMatches do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          match: [ProtoMatch.t()]
        }
  defstruct [:match]

  field :match, 1, repeated: true, type: ProtoMatch
end

defmodule ProtoMatch do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer,
          date: String.t(),
          league: String.t(),
          season: String.t(),
          home: String.t(),
          away: String.t(),
          fthg: integer,
          ftag: integer,
          hthg: integer,
          htag: integer,
          ftr: String.t(),
          htr: String.t()
        }
  defstruct [:id, :date, :league, :season, :home, :away, :fthg, :ftag, :hthg, :htag, :ftr, :htr]

  field :id, 1, type: :int64
  field :date, 2, type: :string
  field :league, 3, type: :string
  field :season, 4, type: :string
  field :home, 5, type: :string
  field :away, 6, type: :string
  field :fthg, 7, type: :int64
  field :ftag, 8, type: :int64
  field :hthg, 9, type: :int64
  field :htag, 10, type: :int64
  field :ftr, 11, type: :string
  field :htr, 12, type: :string
end

defmodule ProtoMatchesProcess do

  @moduledoc """
  This module is complementary modules I made to separate the encoding logic
  from the controller, so all logic relative to Matches protocol buffer is here.
  """

  @doc """
  encode/1 will encode a Enumerable data we retrieved for a query,
  it mean to be used with Soccer.list_matches
  """

  # Encode take a enumarable objet,
  # we throw the raw argument to build during the building of
  # our protocol buffers struct.
  # then we pipe it to encode, and base64 encode so it ready
  # to go through the API render

  def encode(matches) do
    ProtoMatches.new(match: build(matches))
           |> ProtoMatches.encode
           |> Base.encode64
  end

  @doc """
  build, is a staging function that take the raw data parameter from ProtoMatchesProcess.encode
  and make it ready to be use with ProtoMatches.new
  """

  # Build, will loop through our element and create a struct
  # with the ProtoMatch.new function.
  # So it return an array of ProtoMatch struct ready to be
  # embed in ProtoMatches struct

  def build(matches) do
    Enum.map(matches, fn(e) ->
      ProtoMatch.new(
        id: e.id,
        date: e.date,
        league: e.league,
        season: e.season,
        home: e.home,
        away: e.away,
        fthg: e.fthg,
        ftag: e.ftag,
        hthg: e.hthg,
        htag: e.htag,
        ftr: e.ftr,
        htr: e.htr
      )
    end)
  end

end
