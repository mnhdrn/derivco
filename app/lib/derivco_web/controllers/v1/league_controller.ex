defmodule DerivcoWeb.LeagueController do
  use DerivcoWeb, :controller

  alias Derivco.Soccer

  action_fallback DerivcoWeb.FallbackController

  @moduledoc """
  This Module is the controller for our API endpoint api/v1/league || api/v1/leagues
  """

  # This module tend to send to the user all season avalaible in the database
  # It also send an array of all related league for each seasons.

  @doc """
  The index/2 function simply return a list of leagues with an array of related
  seasons, we have in database.
  and launch the railroad/3 function so it render in the correct format
  """

  # format is retrieved by parameter, if it exist we use it,
  # if not we use the JSON format by default
  # In this function we simply call Soccer.list_leagues to get elements.
  # Then we throw our query return to the function railroad for the render

  def index(conn, params) do

    format = params["format"] || "json"
    leagues = Soccer.list_leagues |> format
    railroad(conn, format, leagues)
  end

  @doc """
  This function will take the index/2 retrieved data and format variable to define
  how to render the data.
  """

  # This function will simply check format argument to render the correct data
  # It also encode Protobuf data before render it with text(conn, data)

  def railroad(conn, format, leagues) do
    case format do
      "protobuf" -> text(conn |> put_resp_content_type("application/x-protobuf"),
          ProtoLeaguesProcess.encode(leagues))
     _ -> render(conn, "index.json", leagues: leagues)
    end
  end

  @doc """
  The format/1 function, will take the result of the query and return a more readable
  data stucture for the user to retrieve.
  """

  # first we enumerate the collected element in data, and create a new map with
  # the league, and all data in :season.
  # After we send our new map through Enum.uniq, to avoid repeated items.
  # We re-loop through our new map, and filter the :season field, so it contain
  # only associated league element.
  # Then we Enum.map on the :season again to remove :league field

  def format(data) do
    Enum.map(data, fn(e) ->
      %{league: e.league, season: data}
    end)
    |> Enum.uniq
    |> Enum.map(fn(e) ->
      %{league: String.upcase(e.league),
        season: Enum.filter(e.season, fn (x) -> # here is the tricky part
          if x.league == e.league do true end
        end)
        |> Enum.map(fn(x) -> x.season end)
      }
    end)
  end

end
