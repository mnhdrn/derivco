﻿## Derivco Technical Assessment

_A simple API to retrieve information and results of Football matches._

## Tech/framework used

Obviously I used **Elixir** langage.

**Built with :**
* Phoenix Framework
* Mix Build tool
* Postgres Databases
* [ExDoc](https://github.com/elixir-lang/ex_doc) hex package
* [Credo](https://github.com/rrrene/credo) hex package
* [Protobuf](https://github.com/tony612/protobuf-elixir) hex package
* [Cachex](https://github.com/whitfin/cachex) hex package

## Installation

### requirement :

- #### Development Mode:
	- [Elixir](https://elixir-lang.org/install.html) (Development mode)
	- [PostgreSQL](https://wiki.postgresql.org/wiki/Detailed_installation_guides) (Development mode)

Once every requirements are complete please run the following command to install **Phoenix** framework on your machine for the Development mode.

```
mix local.hex --force
mix local.rebar --force
mix archive.install hex phx_new --force
```

- #### Production Mode:
	- [Docker-ce](https://docs.docker.com/v17.09/engine/installation/#server) (Production mode)
	- [Docker-compose](https://docs.docker.com/compose/install/) (Production mode)

## API References

You can find by default the API references at the root of directory app: 
```
./API_REFERENCES.md
```

The API References is also the landing page of the app, please see the **how to use** section and go on this link: http://localhost

## Documentation

By default a directory call ``` doc ``` will be present in the app directory.

You can find inside a file called ```./index.html```, open it in your browser to access the ExDoc documentation.

You will also find a MarkDown format for the documentation at the root of the app directory.

If the directory is not present, please run the following command in Development mode:

```
mix do deps.get, deps.compile, docs
```

## Test

**requirement**: [Erlang-dialyzer](https://pkgs.org/download/erlang-dialyzer)

Test are provide by ExUnit, Credo and Erlang-dialyzer.

You can run the test in Development mode:

```
MIX_ENV=test mix ecto.setup # This will create database, migrate and seed
mix credo
mix test --cover
mix dialyzer #this one can take a long time to initialize
```

You can also relaunch the pipeline on Gitlab.

## How to use

### Development mode:

Simply enter in ```./app```, launch your postgresql service, ```systemctl start postgresql``` on linux.

then run the following command to initialize the project:

```
mix deps.get #get dependencies packages
mix ecto.setup #Create, Migrate, Seed the database
```

Once it is done just run ```mix phx.server``` and go to your favorite browser at this url: http://localhost:4000

### Production mode:

Make sure that **Docker-ce** and **Docker-compose** requirement are installed.

- #### Initialization:

At the root of the repository, you will find three files, ```docker-compose.yml``` , ```dockerfile``` and ```gen_docker.sh```.

Execute ```./gen_docker.sh my_app MyApp``` to generate config files for **Phoenix** in the purpose to compile our application.

example for this application: ```./gen_docker.sh derivco Derivco```

Then you can tweak build arguments and ENV variables for the docker images of the app in ```docker-compose.yml```, notes that is not recommended.

##### /!\ By default the secret_key build argument is one I generated, you can provide another SECRET_KEY_BASE like that:

```
# in development mode
cd ./app
mix phx.gen.secret
```

Copy the result in your ```docker-compose.yml``` in the build argument.

- #### First launch:

We need to launch it once before being able to scale it correctly, so we can create the docker volume for the database and populate the database.

Run ```docker-compose up --build``` wait for the image to build then try to access the application in your favorite browser at this url: http://localhost

If everything went well you will arrive on the landing page, that means the database is created and migrated.
Sometimes you need to refresh the web page.

Stop your previous ```docker-compose up --build``` by pressing ```ctrl +c```

It's time to seed the database so the API will have some data to play with.

Run ```docker-compose up -d```, wait few seconds and run:
```
# we ask to our compiled and dockerized app to execute an elixir script to populate the database, if it failed that means the migration failed

docker-compose exec web /app/bin/derivco rpc ":code.priv_dir(:derivco) |> Path.join('repo/seeds.exs') |> Code.eval_file()"
```

**This command can take a bit of time to be complete,  ~ 30seconds**

Now please check on your localhost, the database should be seeded, and http://localhost/api/v1/matches must respond a JSON data.

if is not the case you can reset your docker volumes and re-do the all process with the following command

```
docker-compose down --volume
```

You could also try to launch without the ```-d``` flag and run the seed command from another terminal, sometimes it might be more stable !

* #### Other launch and scaling the app:

Simply run this command !

```
docker-compose up -d --scale web=5
```

Note that you can change the scale number to suit your need !

Enjoy, your app is compiled, released, deployed, scaled.

## Notes

This assessment was a great challenge for me because it was my first real API.

I learned about, REST API architecture, writing [documentation in code](https://hexdocs.pm/elixir/writing-documentation.html#documentation-code-comments), writing [test](https://hexdocs.pm/phoenix/testing.html),
caching queries, Erlang node communication, cluster and Term Storage.

The HAProxy configuration in docker-compose was very interesting and I will use it for my own website deployment in near future.

For the documentation, I keep it simple, I try to hide every Phoenix generated module that, I don't explicitly use in this app or where I didn't implement code.

I made this choice for a better clarity and focus on controller, view, protocol buffers modules and context logic I implemented.

Improvement List:
* relational database structure, because flat-structure is easy but is not as modular and performante as a relational data structure. An further more, having flat data structure creates a lot of overhead of the table-context.exs module if you don't create sub-module manually.
* Distributed caching, because we need to update the cache for all node to avoid to repeat some queries. It is important with huge amount of datas.
* Test good practice, I've seen a lot of theorical stuff, QuickCheck, Property-based, Detroit School, London School...
* More accurate CI/CD on gitlab
* Include some metrics package, so we can visualize data over the API logs, usage...etc
* Docker Swarm or Kurbernetes to make the app more scalable.
* Git Hooks, to avoid useless commit, this is a part of CI/CD good practice

## Credits

Clément Richard

contact.clement.richard@gmail.com, [Gitlab](https://gitlab.com/mnhdrn), [linkedin](https://www.linkedin.com/in/clément-richard-186201142)
