defmodule DerivcoWeb.SeasonController do
  use DerivcoWeb, :controller

  alias Derivco.Soccer

  action_fallback DerivcoWeb.FallbackController

  @moduledoc """
  This Module is the controller for our API endpoint api/v1/seasons || api/v1/season
  """

  # This module tend to send to the user all season avalaible in the database
  # It also send an array of all related league for each seasons.

  @doc """
  The index/2 function simply return a list of seasons with an array of
  related league, we have in database.
  Then launch the railroad/3 function so it render in the correct format
  """

  # format is retrieved by parameter, if it exist we use it,
  # if not we use the JSON format by default
  # In this function we simply call Soccer.list_seasons to get elements.
  # Then we throw our query return to the function railroad for the render

  def index(conn, params) do
    format = params["format"] || "json"

    seasons = Soccer.list_seasons |> format
    railroad(conn, format, seasons)
  end

  @doc """
  This function will take the index/2 retrieved data and format variable to define
  how to render the data.
  """

  # This function will simply check format argument to render the correct data
  # It also encode Protobuf data before render it with text(conn, data)

  def railroad(conn, format, seasons) do
    case format do
      "protobuf" -> text(conn |> put_resp_content_type("application/x-protobuf"),
          ProtoSeasonsProcess.encode(seasons))
     _ -> render(conn, "index.json", seasons: seasons)
    end
  end

  @doc """
  The format/1 function, will take the result of the query and return a more readable
  data stucture for the user to retrieve.
  """

  # first we enumerate the collected element in data, and create a new map with
  # the season, and all data in :league.
  # After we send our new map through Enum.uniq, to avoid repeated items.
  # We re-loop through our new map, and filter the :league field, so it contain
  # only associated season element.
  # Then we Enum.map on the :league again to remove :season field

  def format(data) do
    Enum.map(data, fn(e) ->
      %{season: e.season, league: data}
    end)
    |> Enum.uniq
    |> Enum.map(fn(e) ->
      %{season: e.season,
        league: Enum.filter(e.league, fn (x) ->
          if x.season == e.season do true end
        end)
        |> Enum.map(fn(x) -> String.upcase(x.league) end)
      }
    end)
  end

end
